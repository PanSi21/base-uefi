#!/bin/bash
# Script automatizzato creato da PanSi21 per le proprie installazioni.


loadkeys it
ip a
ping 1.1.1.1
timedatectl set-ntp true
timedatectl status
lsblk -f
cfdisk /dev/nvme0n1p1
mkfs.vfat -F32 /dev/nvme0n1p1 -n "EFI-ARCH"

# Encrypt Disk
cryptsetup luksFormat --type luks2 --cipher aes-xts-plain64 --key-size 512 --hash sha512 /dev/nvme0n1p2
cryptsetup luksOpen /dev/nvme0n1p2 ArchCrypt

# Format disk BTRFS
mkfs.btrfs -L "ArchLinux-Btrfs-Encrypt" /dev/mapper/ArchCrypt

# Mount LUKS2
mount /dev/mapper/ArchCrypt /mnt
cd /mnt

# Create SubVol BTRFS
btrfs subvolume create @
btrfs subvolume create @home

cd
umount /mnt

# Mount SubVol BTRFS and EFI
mount -o rw,noatime,ssd,space_cache=v2,compress=zstd,discard,defaults,subvol=@ /dev/mapper/ArchCrypt /mnt
mkdir /mnt/home
mount -o noatime,compress=zstd,discard,defaults,subvol=@home /dev/mapper/ArchCrypt /mnt/home

mkdir -p /mnt/boot/efi
mount /dev/nvme0n1p1 /mnt/boot/efi

# PACSTRAP System and FSTAB
pacstrap /mnt base base-devel linux linux-headers git vim amd-ucode btrfs-progs nvim
genfstab -U /mnt >> /mnt/etc/fstab

#################
# Chroot System #
arch-chroot /mnt

# Cose Basi
loadkeys it
ln -sf /usr/share/zoneinfo/Europe/Rome /etc/localtime
hwclock --systohc
sed -i '297s/.//' /etc/locale.gen
locale-gen
echo "LANG=it_IT.UTF-8" >> /etc/locale.conf
echo "KEYMAP=it" >> /etc/vconsole.conf
echo "Legolas" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 Legolas.localdomain Legolas" >> /etc/hosts

# Add MODULES and HOOKS and create boot image
nvim /etc/mkinitcpio.conf
    # MODULES=(btrfs) //Add btrfs
    # HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck) //Add encrypt
mkinitcpio -p linux

# Install KDE and Base Applications
pacman -S grub grub-btrfs efibootmgr xorg sddm plasma kde-applications firefox simplescreenrecordervlc papirus-icon-thememateria-kde dialog wpa_supplicant wireless_tools plasma-nm openssh dnsutils network-manager-applet

# Install Bootloader
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# Copy UUID Root for FIX - GRUB Bootloader configuration
blkid
    # UUID="2fb43500-ccdf-43f2-b903-26b249e8cae1"

nvim /etc/default/grub
    # GRUB_CMDLINE_LINUX_DEFAULT="loglevel=3 quiet cryptdevice=UUID=8a8c9801-2180-421d-ab98-5b7fa2da9dfa:ArchCrypt root=/dev/mapper/ArchCrypt"
    # GRUB_ENABLE_CRYPTODISK="y"

grub-mkconfig -o /boot/grub/grub.cfg

# Enable Service

systemctl enable sddm
systemctl enable NetworkManager
systemctl enable sshd

# Add USER + visudo
useradd -mG wheel -s /bin/bash pansi21
visudo

# Umount all partitons
printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"