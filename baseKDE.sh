#!/bin/bash
# Script automatizzato creato da PanSi21 per le proprie installazioni.


# Install KDE and Base Applications
pacman -S grub grub-btrfs efibootmgr xorg sddm plasma kde-applications firefox simplescreenrecordervlc papirus-icon-thememateria-kde dialog wpa_supplicant wireless_tools plasma-nm openssh dnsutils network-manager-applet

# Enable Service
systemctl enable sddm
systemctl enable NetworkManager
systemctl enable sshd

# Add MODULES and HOOKS and create boot image
nvim /etc/mkinitcpio.conf
    # MODULES=(btrfs) //Add btrfs
    # HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck) //Add encrypt
mkinitcpio -P

# Add USER + visudo
useradd -mG wheel -s /bin/bash pansi21
visudo
passwd pansi21

# Root password
passwd

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"

# Install Bootloader
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg
